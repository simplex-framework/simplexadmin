@php
    echo "<?php".PHP_EOL;
@endphp

namespace {{ config('simplex-admin.namespace.repository') }};

use {{ config('simplex-admin.namespace.repository') }}\BaseRepository;
use {{ config('simplex-admin.namespace.model') }}\User;

/**
 * Class UserRepository
 * @package {{ config('simplex-admin.namespace.repository') }}
*/

class UserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email',
        'password'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model(): string
    {
        return User::class;
    }
}
