<?php

namespace Tests;

use Simplex\Admin\SimplexAdminServiceProvider;
use Orchestra\Testbench\TestCase as Orchestra;

class TestCase extends Orchestra
{
    protected function getPackageProviders($app)
    {
        return [
            SimplexAdminServiceProvider::class,
        ];
    }
}
