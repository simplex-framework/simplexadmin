<?php

namespace Simplex\Admin;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Simplex\Admin\Commands\API\APIControllerGeneratorCommand;
use Simplex\Admin\Commands\API\APIGeneratorCommand;
use Simplex\Admin\Commands\API\APIRequestsGeneratorCommand;
use Simplex\Admin\Commands\API\TestsGeneratorCommand;
use Simplex\Admin\Commands\APIScaffoldGeneratorCommand;
use Simplex\Admin\Commands\Common\MigrationGeneratorCommand;
use Simplex\Admin\Commands\Common\ModelGeneratorCommand;
use Simplex\Admin\Commands\Common\RepositoryGeneratorCommand;
use Simplex\Admin\Commands\Publish\GeneratorPublishCommand;
use Simplex\Admin\Commands\Publish\PublishTablesCommand;
use Simplex\Admin\Commands\Publish\PublishUserCommand;
use Simplex\Admin\Commands\RollbackGeneratorCommand;
use Simplex\Admin\Commands\Scaffold\ControllerGeneratorCommand;
use Simplex\Admin\Commands\Scaffold\RequestsGeneratorCommand;
use Simplex\Admin\Commands\Scaffold\ScaffoldGeneratorCommand;
use Simplex\Admin\Commands\Scaffold\ViewsGeneratorCommand;
use Simplex\Admin\Core\FileSystem;
use Simplex\Admin\Core\GeneratorConfig;
use Simplex\Admin\Generators\API\APIControllerGenerator;
use Simplex\Admin\Generators\API\APIRequestGenerator;
use Simplex\Admin\Generators\API\APIRoutesGenerator;
use Simplex\Admin\Generators\API\APITestGenerator;
use Simplex\Admin\Generators\FactoryGenerator;
use Simplex\Admin\Generators\MigrationGenerator;
use Simplex\Admin\Generators\ModelGenerator;
use Simplex\Admin\Generators\RepositoryGenerator;
use Simplex\Admin\Generators\RepositoryTestGenerator;
use Simplex\Admin\Generators\Scaffold\ControllerGenerator;
use Simplex\Admin\Generators\Scaffold\MenuGenerator;
use Simplex\Admin\Generators\Scaffold\RequestGenerator;
use Simplex\Admin\Generators\Scaffold\RoutesGenerator;
use Simplex\Admin\Generators\Scaffold\ViewGenerator;
use Simplex\Admin\Generators\SeederGenerator;

class SimplexAdminServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $configPath = __DIR__.'/../config/simplex-admin.php';
            $this->publishes([
                $configPath => config_path('simplex-admin.php'),
            ], 'simplex-admin-config');

            $this->publishes([
                __DIR__.'/../views' => resource_path('views/vendor/simplex-admin'),
            ], 'simplex-admin-templates');
        }

        $this->registerCommands();
        $this->loadViewsFrom(__DIR__.'/../views', 'simplex-admin');

        View::composer('*', function ($view) {
            $view->with(['config' => app(GeneratorConfig::class)]);
        });

        Blade::directive('tab', function () {
            return '<?php echo simplex_tab() ?>';
        });

        Blade::directive('tabs', function ($count) {
            return "<?php echo simplex_tabs($count) ?>";
        });

        Blade::directive('nl', function () {
            return '<?php echo simplex_nl() ?>';
        });

        Blade::directive('nls', function ($count) {
            return "<?php echo simplex_nls($count) ?>";
        });
    }

    private function registerCommands()
    {
        if (!$this->app->runningInConsole()) {
            return;
        }

        $this->commands([
            APIScaffoldGeneratorCommand::class,

            APIGeneratorCommand::class,
            APIControllerGeneratorCommand::class,
            APIRequestsGeneratorCommand::class,
            TestsGeneratorCommand::class,

            MigrationGeneratorCommand::class,
            ModelGeneratorCommand::class,
            RepositoryGeneratorCommand::class,

            GeneratorPublishCommand::class,
            PublishTablesCommand::class,
            PublishUserCommand::class,

            ControllerGeneratorCommand::class,
            RequestsGeneratorCommand::class,
            ScaffoldGeneratorCommand::class,
            ViewsGeneratorCommand::class,

            RollbackGeneratorCommand::class,
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/simplex-admin.php', 'simplex-admin');

        $this->app->singleton(GeneratorConfig::class, function () {
            return new GeneratorConfig();
        });

        $this->app->singleton(FileSystem::class, function () {
            return new FileSystem();
        });

        $this->app->singleton(MigrationGenerator::class);
        $this->app->singleton(ModelGenerator::class);
        $this->app->singleton(RepositoryGenerator::class);

        $this->app->singleton(APIRequestGenerator::class);
        $this->app->singleton(APIControllerGenerator::class);
        $this->app->singleton(APIRoutesGenerator::class);

        $this->app->singleton(RequestGenerator::class);
        $this->app->singleton(ControllerGenerator::class);
        $this->app->singleton(ViewGenerator::class);
        $this->app->singleton(RoutesGenerator::class);
        $this->app->singleton(MenuGenerator::class);

        $this->app->singleton(RepositoryTestGenerator::class);
        $this->app->singleton(APITestGenerator::class);

        $this->app->singleton(FactoryGenerator::class);
        $this->app->singleton(SeederGenerator::class);
    }
}
