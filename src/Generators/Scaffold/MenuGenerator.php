<?php

namespace Simplex\Admin\Generators\Scaffold;

use Illuminate\Support\Str;
use Simplex\Admin\Generators\BaseGenerator;
class MenuGenerator extends BaseGenerator
{
    private string $templateType;

    public function __construct()
    {
        parent::__construct();

        $this->path = config('simplex-admin.path.menu_file', resource_path('views/layouts/menu.blade.php'));
        $this->templateType = config('simplex-admin.templates', 'adminlte-templates');
    }

    public function generate()
    {
        $menuContents = g_filesystem()->getFile($this->path);

        $menu = view($this->templateType.'::templates.layouts.menu_template')->render();

        if (Str::contains($menuContents, $menu)) {
            $this->config->commandInfo(simplex_nl().'Menu '.$this->config->modelNames->humanPlural.' already exists, Skipping Adjustment.');

            return;
        }

        $menuContents .= simplex_nl().$menu;

        g_filesystem()->createFile($this->path, $menuContents);
        $this->config->commandComment(simplex_nl().$this->config->modelNames->dashedPlural.' menu added.');
    }

    public function rollback()
    {
        $menuContents = g_filesystem()->getFile($this->path);

        $menu = view($this->templateType.'::templates.layouts.menu_template')->render();

        if (Str::contains($menuContents, $menu)) {
            g_filesystem()->createFile($this->path, str_replace($menu, '', $menuContents));
            $this->config->commandComment('menu deleted');
        }
    }
}
