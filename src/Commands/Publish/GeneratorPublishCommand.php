<?php

namespace Simplex\Admin\Commands\Publish;

use Symfony\Component\Console\Input\InputOption;

class GeneratorPublishCommand extends PublishBaseCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'simplex:publish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publishes & init api routes, base controller, base test cases traits.';

    public function handle()
    {
        $this->updateRouteServiceProvider();
        $this->publishTestCases();
        $this->publishBaseController();
        $repositoryPattern = config('simplex-admin.options.repository_pattern', true);
        if ($repositoryPattern) {
            $this->publishBaseRepository();
        }
        if ($this->option('localized')) {
            $this->publishLocaleFiles();
        }
    }

    private function updateRouteServiceProvider()
    {
        $routeServiceProviderPath = app_path('Providers'.DIRECTORY_SEPARATOR.'RouteServiceProvider.php');

        if (!file_exists($routeServiceProviderPath)) {
            $this->error("Route Service provider not found on $routeServiceProviderPath");

            return;
        }

        $fileContent = g_filesystem()->getFile($routeServiceProviderPath);

        $search = "Route::middleware('api')".simplex_nl().str(' ')->repeat(16)."->prefix('api')";
        $beforeContent = str($fileContent)->before($search);
        $afterContent = str($fileContent)->after($search);

        $finalContent = $beforeContent.$search.simplex_nl().str(' ')->repeat(16)."->as('api.')".$afterContent;
        g_filesystem()->createFile($routeServiceProviderPath, $finalContent);
    }

    private function publishTestCases()
    {
        $testsPath = config('simplex-admin.path.tests', base_path('tests/'));
        $testsNameSpace = config('simplex-admin.namespace.tests', 'Tests');
        $createdAtField = config('simplex-admin.timestamps.created_at', 'created_at');
        $updatedAtField = config('simplex-admin.timestamps.updated_at', 'updated_at');

        $templateData = view('simplex-admin::api.test.api_test_trait', [
            'timestamps'      => "['$createdAtField', '$updatedAtField']",
            'namespacesTests' => $testsNameSpace,
        ])->render();

        $fileName = 'ApiTestTrait.php';

        if (file_exists($testsPath.$fileName) && !$this->confirmOverwrite($fileName)) {
            return;
        }

        g_filesystem()->createFile($testsPath.$fileName, $templateData);
        $this->info('ApiTestTrait created');

        $testAPIsPath = config('simplex-admin.path.api_test', base_path('tests/APIs/'));
        if (!file_exists($testAPIsPath)) {
            g_filesystem()->createDirectoryIfNotExist($testAPIsPath);
            $this->info('APIs Tests directory created');
        }

        $testRepositoriesPath = config('simplex-admin.path.repository_test', base_path('tests/Repositories/'));
        if (!file_exists($testRepositoriesPath)) {
            g_filesystem()->createDirectoryIfNotExist($testRepositoriesPath);
            $this->info('Repositories Tests directory created');
        }
    }

    private function publishBaseController()
    {
        $controllerPath = app_path('Http/Controllers/');
        $fileName = 'AppBaseController.php';

        if (file_exists($controllerPath.$fileName) && !$this->confirmOverwrite($fileName)) {
            return;
        }

        $templateData = view('simplex-admin::stubs.app_base_controller', [
            'namespaceApp' => $this->getLaravel()->getNamespace(),
            'apiPrefix'    => config('simplex-admin.api_prefix'),
        ])->render();

        g_filesystem()->createFile($controllerPath.$fileName, $templateData);

        $this->info('AppBaseController created');
    }

    private function publishBaseRepository()
    {
        $repositoryPath = app_path('Repositories/');

        $fileName = 'BaseRepository.php';

        if (file_exists($repositoryPath.$fileName) && !$this->confirmOverwrite($fileName)) {
            return;
        }

        g_filesystem()->createDirectoryIfNotExist($repositoryPath);

        $templateData = view('simplex-admin::stubs.base_repository', [
            'namespaceApp' => $this->getLaravel()->getNamespace(),
        ])->render();

        g_filesystem()->createFile($repositoryPath.$fileName, $templateData);

        $this->info('BaseRepository created');
    }

    private function publishLocaleFiles()
    {
        $localesDir = __DIR__.'/../../../locale/';

        $this->publishDirectory($localesDir, lang_path(), 'lang', true);

        $this->comment('Locale files published');
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    public function getOptions()
    {
        return [
            ['localized', null, InputOption::VALUE_NONE, 'Localize files.'],
        ];
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }
}
