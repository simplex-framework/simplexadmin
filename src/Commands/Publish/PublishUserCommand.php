<?php

namespace Simplex\Admin\Commands\Publish;


use Simplex\Admin\Commands\Publish\PublishBaseCommand;

class PublishUserCommand extends PublishBaseCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'simplex.publish:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publishes Users CRUD file';

    public function handle()
    {
        $this->copyViews();
        $this->updateRoutes();
        $this->updateMenu();
        $this->publishUserController();
        if (config('simplex-admin.options.repository_pattern')) {
            $this->publishUserRepository();
        }
        $this->publishCreateUserRequest();
        $this->publishUpdateUserRequest();
    }

    private function copyViews()
    {
        $viewsPath = config('simplex-admin.path.views', resource_path('views/'));
        $templateType = config('simplex-admin.templates', 'adminlte-templates');

        $this->createDirectories($viewsPath.'users');

        $files = $this->getViews();

        foreach ($files as $templateView => $destinationView) {
            $content = view($templateType.'::'.$templateView);
            $destinationFile = $viewsPath.$destinationView;
            g_filesystem()->createFile($destinationFile, $content);
        }
    }

    private function createDirectories($dir)
    {
        g_filesystem()->createDirectoryIfNotExist($dir);
    }

    private function getViews(): array
    {
        return [
            'users.create'      => 'users/create.blade.php',
            'users.edit'        => 'users/edit.blade.php',
            'users.fields'      => 'users/fields.blade.php',
            'users.index'       => 'users/index.blade.php',
            'users.show'        => 'users/show.blade.php',
            'users.show_fields' => 'users/show_fields.blade.php',
            'users.table'       => 'users/table.blade.php',
        ];
    }

    private function updateRoutes()
    {
        $path = config('simplex-admin.path.routes', base_path('routes/web.php'));

        $routeContents = g_filesystem()->getFile($path);
        $controllerNamespace = config('simplex-admin.namespace.controller');
        $routeContents .= simplex_nls(2)."Route::resource('users', '$controllerNamespace\UserController')->middleware('auth');";

        g_filesystem()->createFile($path, $routeContents);
        $this->comment("\nUser route added");
    }

    private function updateMenu()
    {
        $viewsPath = config('simplex-admin.path.views', resource_path('views/'));
        $templateType = config('simplex-admin.templates', 'adminlte-templates');
        $path = $viewsPath.'layouts/menu.blade.php';
        $menuContents = g_filesystem()->getFile($path);
        $usersMenuContent = view($templateType.'::templates.users.menu')->render();
        $menuContents .= simplex_nl().$usersMenuContent;

        g_filesystem()->createFile($path, $menuContents);
        $this->comment("\nUser Menu added");
    }

    private function publishUserController()
    {
        $name = 'user_controller';

        if (!config('simplex-admin.options.repository_pattern')) {
            $name = 'user_controller_without_repository';
        }

        $controllerPath = config('simplex-admin.path.controller', app_path('Http/Controllers/'));
        $controllerPath .= 'UserController.php';

        $controllerContents = view('simplex-admin::scaffold.user.'.$name)->render();

        g_filesystem()->createFile($controllerPath, $controllerContents);

        $this->info('UserController created');
    }

    private function publishUserRepository()
    {
        $repositoryPath = config('simplex-admin.path.repository', app_path('Repositories/'));

        $fileName = 'UserRepository.php';

        g_filesystem()->createDirectoryIfNotExist($repositoryPath);

        if (file_exists($repositoryPath.$fileName) && !$this->confirmOverwrite($fileName)) {
            return;
        }

        $templateData = view('simplex-admin::scaffold.user.user_repository')->render();
        g_filesystem()->createFile($repositoryPath.$fileName, $templateData);

        $this->info('UserRepository created');
    }

    private function publishCreateUserRequest()
    {
        $requestPath = config('simplex-admin.path.request', app_path('Http/Requests/'));

        $fileName = 'CreateUserRequest.php';

        g_filesystem()->createDirectoryIfNotExist($requestPath);

        if (file_exists($requestPath.$fileName) && !$this->confirmOverwrite($fileName)) {
            return;
        }

        $templateData = view('simplex-admin::scaffold.user.create_user_request')->render();
        g_filesystem()->createFile($requestPath.$fileName, $templateData);

        $this->info('CreateUserRequest created');
    }

    private function publishUpdateUserRequest()
    {
        $requestPath = config('simplex-admin.path.request', app_path('Http/Requests/'));

        $fileName = 'UpdateUserRequest.php';
        if (file_exists($requestPath.$fileName) && !$this->confirmOverwrite($fileName)) {
            return;
        }

        $templateData = view('simplex-admin::scaffold.user.update_user_request')->render();
        g_filesystem()->createFile($requestPath.$fileName, $templateData);

        $this->info('UpdateUserRequest created');
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    public function getOptions()
    {
        return [];
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }
}
